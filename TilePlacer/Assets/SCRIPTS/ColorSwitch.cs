﻿using UnityEngine;

public class ColorSwitch : MonoBehaviour
{
    public TileManager tileManager;

    public Material redMat;
    public Material blueMat;
    public Material greenMat;
    public Material yellowMat;
    public Material purpleMat;
    
    public Material whiteMat;

    static Material[] materials;

    public static Material activeColor;
    
    public static int activeColorIndex;

    void Start()
    {
        materials = new Material[] { redMat, blueMat, greenMat, yellowMat, purpleMat, whiteMat };

        activeColor = materials[5];
    }

    public void SwitchColor(int _colorID)
    {
        activeColor = materials[_colorID];

        tileManager.ChangeMaterial();

        activeColorIndex = _colorID;
    }
}
