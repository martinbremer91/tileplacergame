﻿using UnityEngine;

public class Tile : MonoBehaviour
{
    public BoardManager boardManager;

    public MeshRenderer[] meshRenderers;
    public Transform[] squares;

    public void ChangeMaterial()
    {
        foreach (MeshRenderer mr in meshRenderers)
            mr.material = ColorSwitch.activeColor;
    }

    public void ScanBoard()
    {
        int index = ColorSwitch.activeColorIndex;
        
        boardManager.ClearBoard();

        foreach (Transform square in squares)
        {
            Ray ray = new Ray(square.position, Vector3.forward);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    BoardSquare detectedObj = hit.collider.gameObject.GetComponent<BoardSquare>();

                    if (index != 5 && !detectedObj.occupied)
                        detectedObj.ChangeMaterial();
                    else
                        detectedObj.clear = true;
                }
                else
                    return;
            }
        }
    }
}
