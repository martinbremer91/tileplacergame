﻿using UnityEngine;

public class TileManager : MonoBehaviour
{
    public Camera cam;

    public Transform tileHolder;

    public BoardManager boardManager;

    public Tile iDisplay;
    public Tile lDisplay;
    public Tile oDisplay;
    public Tile sDisplay;
    public Tile tDisplay;
    
    public Tile singleSquareDisplay;

    Tile[] displays;

    public Tile iTile;
    public Tile lTile;
    public Tile oTile;
    public Tile sTile;
    public Tile tTile;

    public Tile singleSquare;

    Tile[] tiles;

    int activeTileIndex;

    public static bool selected;

    void Start()
    {
        displays = new Tile[] { iDisplay, lDisplay, oDisplay, sDisplay, tDisplay, singleSquareDisplay };
        tiles = new Tile[] { iTile, lTile, oTile, sTile, tTile, singleSquare };
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
            FlipTile();

        if (Input.GetKeyDown(KeyCode.R))
            RotateTile();

        if (selected)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                tiles[activeTileIndex].transform.position = new Vector3(hit.point.x, hit.point.y, -2);

                tiles[activeTileIndex].ScanBoard();
            }

            if (Input.GetMouseButtonDown(0))
                OccupySquares();

            if (Input.GetMouseButtonUp(1))
                DeselectTile();
        }
    }

    public void DisplayTile(int _id)
    {
        displays[activeTileIndex].gameObject.SetActive(false);

        activeTileIndex = _id;

        displays[activeTileIndex].gameObject.SetActive(true);

        ChangeMaterial();
    }

    public void ChangeMaterial()
    {
        displays[activeTileIndex].ChangeMaterial();
        tiles[activeTileIndex].ChangeMaterial();
        singleSquareDisplay.ChangeMaterial();
    }

    public void SelectTile()
    {
        selected = true;
    }

    void DeselectTile()
    {
        selected = false;
        tiles[activeTileIndex].transform.position = tileHolder.position;

        tiles[activeTileIndex].ScanBoard();
    }

    void OccupySquares()
    {
        selected = false;
        tiles[activeTileIndex].transform.position = tileHolder.position;

        boardManager.CheckOccupiedSquares();        
    }

    void FlipTile()
    {
        displays[activeTileIndex].transform.Rotate(new Vector3(180, 0, 0));
        tiles[activeTileIndex].transform.Rotate(new Vector3(180, 0, 0));
    }

    void RotateTile()
    {
        displays[activeTileIndex].transform.Rotate(new Vector3(0, 0, 90));
        tiles[activeTileIndex].transform.Rotate(new Vector3(0, 0, 90));
    }
}
