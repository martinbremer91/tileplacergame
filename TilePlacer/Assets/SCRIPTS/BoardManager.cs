﻿using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public BoardSquare[] squares;

    public void ClearBoard()
    {
        foreach (BoardSquare square in squares)
        {
            square.ResetSquare();
        }
    }

    public void CheckOccupiedSquares()
    {
        foreach (BoardSquare square in squares)
        {
            square.OccupySquare();
        }
    }
}
