﻿using System.Collections.Generic;
using UnityEngine;

public enum cardIDs { I, L, O, S, T }
public class DeckManager : MonoBehaviour
{
    public TileManager tileManager;

    public int deckSize;
    cardIDs[] orderedDeck;
    cardIDs[] shuffledDeck;

    int cardSetSize;

    int cardCounter;

    void Start()
    {
        if (deckSize == 0 || deckSize % 5 != 0)
        {
            Debug.LogError("Invalid deck size. Must be multiple of five");
            return;
        }

        cardSetSize = deckSize / 5;

        orderedDeck = new cardIDs[deckSize];
        shuffledDeck = new cardIDs[deckSize];

        GenerateOrderedDeck();
        ShuffleDeck();
    }

    void GenerateOrderedDeck()
    {
        cardIDs id = 0;

        for (int i = 0; i < deckSize; i++)
        {
            if (i != 0 && i % cardSetSize == 0)
                id++;

            orderedDeck[i] = id;
        }
    }

    void ShuffleDeck()
    {
        List<cardIDs> bufferDeck = new List<cardIDs>();

        foreach (cardIDs card in orderedDeck)
            bufferDeck.Add(card);

        for (int i = 0; i < deckSize; i++)
        {
            int randomIndex = Random.Range(0, bufferDeck.Count);

            shuffledDeck[i] = bufferDeck[randomIndex];
            bufferDeck.RemoveAt(randomIndex);
        }
    }

    public void DrawCard()
    {
        if (cardCounter < deckSize)
        {
            tileManager.DisplayTile((int)shuffledDeck[cardCounter]);

            cardCounter++;
        }
    }
}
