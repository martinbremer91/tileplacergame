﻿using UnityEngine;

public class BoardSquare : MonoBehaviour
{
    public MeshRenderer mr;
    public Material whiteMat;

    public bool occupied;
    public bool clear;

    public void ChangeMaterial()
    {
        mr.material = ColorSwitch.activeColor;

        clear = false;
    }

    public void ResetSquare()
    {
        if (!occupied && !clear)
        {
            clear = true;
            mr.material = whiteMat;
        }

        if (occupied)
            clear = false;
    }

    public void OccupySquare()
    {
        occupied = !clear;

        if (ColorSwitch.activeColorIndex == 5 && !occupied)
            mr.material = ColorSwitch.activeColor;
    }
}
